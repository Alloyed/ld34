local class     = require 'gabe.class'
local letterbox = require 'gfx.letterbox'
local misc      = require 'misc'
local lg        = require 'love.graphics'

local function grid(camera)
	lg.push'all'
	local cx, cy, scale = camera:unpack()
	local w, h = letterbox.dimensions()
	local step = Config.tileSize * scale
	cx = cx * scale
	cy = cy * scale

	lg.setColor(50, 50, 50)
	lg.setLineWidth(1)

	cx = -cx % step
	for x=cx, w, step do
		lg.line(x, 0, x, h)
	end

	cy = -cy % step
	for y=cy, h, step do
		lg.line(0, y, w, y)
	end
	lg.pop()
end

local function fps()
	local msg = "FPS: " .. love.timer.getFPS()
	if A.client and A.client.server then
		msg = msg .. " ping: " .. math.ceil(A.client.server:round_trip_time()/2)
	end
	local w, h = letterbox.dimensions()
	local fw = A.debug_font:getWidth(msg)
	local fh = A.debug_font:getHeight()
	fw = fw + 4
	fh = fh + 4
	local x = w - fw
	local y = 0

	lg.setColor(0, 0, 0)
	lg.rectangle('fill', x, y, fw, fh)
	lg.setFont(A.debug_font)
	lg.setColor(255, 255, 255)
	lg.print(msg, x + 2, y + 2)
end


local function outline(x, y, w, h)
	love.graphics.rectangle('line', x, y, w, h)
	--love.graphics.line(x,   y,   x+w, y)
	--love.graphics.line(x,   y+h, x+w, y+h)
	--love.graphics.line(x,   y,   x,   y+h)
	--love.graphics.line(x+w, y,   x+w, y+h)
end

local function ctrl()
	return love.keyboard.isDown'lctrl' or love.keyboard.isDown'rctrl'
end

local function shift()
	return love.keyboard.isDown'lshift' or love.keyboard.isDown'rshift'
end

local function to_grid(x, y)
	local ts = Config.tileSize
	return math.floor(x / ts) * ts, math.floor(y / ts) * ts 
end

local function bounding_box(cstart, cend)
	local x, y   = unpack(cstart)
	local ex, ey = unpack(cend)
	if ex < x then
		return bounding_box({ex, y}, {x, ey})
	elseif ey < y then
		return bounding_box({x, ey}, {ex, y})
	end
	return x, y, ex - x, ey - y
end

local function grid_box(cstart, cend)
	local x, y   = unpack(cstart)
	local ex, ey = unpack(cend)
	if ex < x then
		return grid_box({ex, y}, {x, ey})
	elseif ey < y then
		return grid_box({x, ey}, {ex, y})
	end
	local ts = Config.tileSize
	x, y = to_grid(x, y)
	ex, ey = to_grid(ex + ts, ey + ts)
	return x, y, ex - x, ey - y
end

local function draw_mouse(x, y)
	local x, y = to_grid(x, y)
	lg.setColor(200, 200, 200)
	local hts = Config.tileSize / 2
	lg.circle('line', x+hts, y+hts, hts, 20)
end

local function popup(msg)
	local fw = A.debug_font:getWidth(msg)
	local fh = A.debug_font:getHeight() * (misc.cmatch(msg, '\n') + 1)
	local cx, cy = letterbox.dimensions()
	cx = cx / 2
	cy = cy / 2
	local x, y = math.floor(cx - (fw/2)), math.floor(cy - (fh/2))

	lg.setColor(0, 0, 0)
	lg.rectangle('fill', x - 5, y - 5, fw + 10, fh + 10)

	lg.setFont(A.debug_font)
	lg.setColor(255, 255, 255)
	lg.print(msg, x, y)
end


-- right-aligned print
local function rprint(msg, x, y)
	local fw = A.debug_font:getWidth(msg)
	lg.print(msg, x - fw, y)
end

-- down-aligned print
local function dprint(msg, x, y)
	local fh = A.debug_font:getHeight()
	lg.print(msg, x, y - fh)
end


return {
	grid         = grid,
	fps          = fps,
	outline      = outline,
	ctrl         = ctrl,
	shift        = shift,
	to_grid      = to_grid,
	bounding_box = bounding_box,
	grid_box     = grid_box,
	draw_mouse   = draw_mouse,
	popup        = popup,
	rprint       = rprint,
	dprint       = dprint,
}
