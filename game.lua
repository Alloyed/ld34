local Block = require 'block'
local Player = require 'player'
local class = require 'gabe.class'
local lg = require 'love.graphics'
local bump = require 'bump'
local Screen = require 'letterbox'
local flux = require 'flux'

local game = class 'game'

function game:update(real_dt)
	if S.paused then
		return
	end

	if not S.player.dead and not A.song:isPlaying() and not A.song_loop:isPlaying() then
		A.song_loop:rewind()
		A.song_loop:setLooping(true)
		A.song_loop:setPitch(A.song:getPitch())
		A.song_loop:play()
	end

	local c  = clock()
	local dt = translate_dt(real_dt)
	flux.update(dt)

	local w, h = Screen.dimensions()

	if S.vshake then
		S.vshake = S.vshake - dt
		if S.vshake < 0 then
			S.vshake = nil
		end
	end

	if S.hshake then
		S.hshake = S.hshake - dt
		if S.hshake < 0 then
			S.hshake = nil
		end
	end

	S.spawn = S.spawn + dt
	if S.spawn > Config.spawn_rate then
		S.spawn = S.spawn - Config.spawn_rate
		A.patterns:spawn()
	end

	S.difficulty_timer = S.difficulty_timer + dt
	if S.difficulty_timer > Config.spawn_rate then

		if not S.player.dead then
			S.difficulty = math.min(
				Config.max_difficulty,
				S.difficulty + Config.difficulty_step)
			if Config.hardmode then
				A.song:setPitch((S.difficulty - 1) * .1 + 1)
			end
			if not S.player.last_crouched then
				S.player.w = Config.player_w * S.difficulty
				S.player.h = Config.player_h * S.difficulty
				S.player:resize()
			end
		end

		S.difficulty_timer = S.difficulty_timer - Config.spawn_rate
	end
		

	if not S.player.dead then
		S.player:update(dt, c)
		S.score = S.score + dt
		if Config.hardmode then
			S.score = S.score + (dt * .5)
		end
	end

	-- so we can remove blocks inline
	for i = #S.blocks, 1, -1 do
		S.blocks[i]:update(dt, c)
	end
end

local palette = require 'palette'

local function lines(c)
	local w, h = Screen.dimensions()
	lg.push()
		lg.setColor(palette.background_2)
		for i=1, 5 do
			lg.rectangle('fill', 0, 0, 36, h)
			lg.translate(72, 0)
		end
	lg.pop()
end

function game:scene(c)
	-- you can pause and get infinite screenshake if you feel like it.
	-- Too lazy to fix
	if S.vshake then
		lg.translate(0, math.random(-2, 2))
	end

	if S.hshake then
		lg.translate(math.random(-1, 1), 0)
	end
	local w, h = Screen.dimensions()
	lg.translate(w*.5, h*.5)
	lg.scale((c-math.floor(c)) * .015 + 1)
	lg.translate(w*-.5, h*-.5)
	lines(c)

	lg.setBlendMode('alpha', true)
	for _, b in ipairs(S.blocks) do
		b:draw_trail(c)
	end

	S.player:draw(c)

	for _, b in ipairs(S.blocks) do
		lg.push('all')
		b:draw(c)
		lg.pop()
	end
end


local function printc(s, x, y, f)
	if not f then
		f = love.graphics.getFont()
	end
	local fw = f:getWidth(s)
	lg.print(s, x - fw*.5, y)
end

function game:draw()
	local w, h = Screen.dimensions()
	local c = clock()

	if Config.bloom then
		A.bloom:predraw()
		A.bloom:enabledrawtobloom()
		lg.push()
		self:scene(c)
		lg.pop()
		A.bloom:postdraw()
	end
	lg.push()
	self:scene(c)
	lg.pop()

	lg.setFont(A.dbg_font)

	if S.muted then
		lg.setColor(255, 255, 255)
		lg.print("muted", 10, 10)
	end

	local controls, upload = "", ""
	if love.system.getOS() == "Android" then
		controls = "Tap to restart"
	else
		controls = "Press Space/A\nto restart\n"
		upload = "Press U/Select\nto upload score"
	end

	-- holy wtf batman
	lg.setColor(255, 255, 255)
	lg.setFont(A.menu_font)
	if S.paused then
		printc("PAUSED", 180, 150)
		printc("Press Space/A\nto unpause", 180, 150 + 34)
	elseif S.name then
		printc("Enter your name:", 180, 150)
		s = S.name
		lg.print(s, 10, 200)
	elseif S.leaderboard or (S.player.dead and S.channel) then
		printc("LEADERBOARDS", 180, 150)
		require 'ui.leaderboard'(controls, w)
	elseif S.player.dead and S.score > S.high_score then
		printc("NEW HIGH SCORE!", 180, 150)
		printc(controls .. "\n" .. upload, 180, 150 + 34)
	elseif S.player.dead then
		printc("GAME OVER", 180, 150)
		printc(controls, 180, 150 + 34)
	end

	lg.setColor(255, 255, 255)
	lg.setFont(A.menu_font)
	local s = string.format("%04d", S.score)
	local fw = A.menu_font:getWidth(s)
	if S.score > S.high_score then
		s = s .. "!"
	end
	lg.print(s, 180 - fw*.5, 20)
	lg.print("" .. love.timer.getFPS(), 180 - fw*.5, 40)
end

function game:start()
	A.song:stop()
	A.song:rewind()
	--A.song:setLooping(true)
	A.song:setPitch(1)
	A.song:play()

	S.paused = false
	S.difficulty = Config.starting_difficulty
	S.world = bump.newWorld()

	S.blocks = {}
	S.score = 0
	S.spawn = 0
	S.difficulty_timer = -2

	S.player = Player.new(100, 740, Config.player_w, Config.player_h)
	flux.to(S.player, 2, {cy = Config.player_y})
	A.screen.bgcolor = palette.background_1

	local w, h = Screen.dimensions()
	S.world:add("left_wall", -100, 0, 100, h)
	S.world:add("right_wall", w,  0, 100, h)
end

function game:stop()
end

local function ctrl()
	return love.keyboard.isDown('lctrl') or love.keyboard.isDown('rctrl')
end

local function send_score()
	local th = love.thread.newThread("request.lua")
	S.channel = love.thread.newChannel()
	th:start(S.channel, Config.username, S.score)
end

function game:keypressed(k)
	if not S.name then return false end

	if k == 'backspace' and ctrl() then
		S.name = ""
	elseif k == 'backspace' then
		S.name = string.sub(S.name, 1, -2)
	elseif k == 'return' then
		Config.username = S.name
		S.name = nil
		love.keyboard.setKeyRepeat(false)
		send_score()
	elseif k == 'escape' then
		S.name = nil
	else
		return false
	end

	return true
end

function game:textinput(k)
	if S.name then
		S.name = S.name .. k
		return true
	end

	return false
end

return game
