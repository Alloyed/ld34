local class = require 'gabe.class'
local misc  = require 'misc'
local lg    = require 'love.graphics'

local Button = class 'ui.button'

function Button:init(x, y)
	assert(self.event)

	local fw = A.menu_font:getWidth(self.msg)
	local fh = A.menu_font:getHeight() * (misc.cmatch(self.msg, '\n') + 1)

	self.y = y

	self.w = fw + 10
	self.h = fh + 10
	assert(self.w)
	assert(self.h)

	self.x = x - self.w * .5
end

function Button:update(_, dt, x, y)
	if not x then
		x, y = -1, -1
	end
	if misc.in_box(x, y, misc.bbox(self)) then
		return true
	end
end

function Button:draw()
	lg.push'all'

	--lg.setColor(0, 0, 0)
	--self.x = -self.w * .5
	--self.y = 10
	--lg.rectangle('fill', self.x, self.y, self.w, self.h)

	lg.setFont(A.menu_font)
	--lg.setColor(255, 255, 255)
	lg.print(self.msg, self.x, self.y)

	lg.pop()
end

function Button:click(x, y, btn)
	if btn == 1 and misc.in_box(x, y, misc.bbox(self)) then
		return true
	end
	return false
end

function Button:setMsg(msg)
	if self.msg == msg then return end
	self.msg = msg

	local fw = A.menu_font:getWidth(msg)
	local fh = A.menu_font:getHeight() * (misc.cmatch(msg, '\n') + 1)

	self.w = fw + 10
	self.h = fh + 10
end

function Button.extend(msg, event)
	local name = "ui.button." .. msg
	local Btn = class(name)
	Btn.msg = msg

	Btn.event = event
	class.mixin(Btn, Button)
	return Btn
end

return Button
