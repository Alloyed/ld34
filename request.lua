local channel, username, score = ...

local http = require 'socket.http'
local url  = require 'socket.url'
local ltn12 = require 'ltn12'

assert(type(username) == 'string')
assert(type(score) == 'number')

local t = {}

local uname_enc = url.escape(username)
local score_enc = url.escape(tostring(score))


local u = url.build {
	scheme = "http",
	host = "ld34.alloyed.me",
	--host = "localhost", port = "3000",
	path = "/score",
	query = string.format("username=%s&score=%s", uname_enc, score_enc)
}


local body, status, headers = http.request {
	url = u,
	method = "POST",
	sink = ltn12.sink.table(t)
}

if body == 1 then
	channel:supply { json = table.concat(t) }
else
	channel:supply { err = status }
end
