local class = require 'gabe.class'
local lfs = require 'love.filesystem'

local patterns = class 'patterns'
local Coin = require 'coin'

function patterns:init()
	local data   = {}

	local normal = {}
	local hard   = {}
	local rare   = {}

	for _, fname in ipairs(lfs.getDirectoryItems("art/patterns")) do
		if fname:match("%.png$") then
			data[fname] = love.image.newImageData("art/patterns/"..fname)
			if fname:match("_hard") then
				table.insert(hard, fname)
			elseif fname:match("_rare") then
				table.insert(rare, fname)
			else
				table.insert(normal, fname)
			end
		end
	end
	self.data = data

	self.normal = normal
	self.rare = rare
	self.hard = hard

	self.last = ""
end

local Block = require 'block'

local function white(r, g, b, a)
	return a ~= 0 and r > 200
end

local function black(r, g, b, a)
	return a ~= 0 and r < 10
end

local function gp(image, used, x, y)
	if used[x][y] then
		return false
	end

	return white(image:getPixel(x, y))
end

local function use(used, x, w, y, h)
	for ix=0, w-1 do
		for iy=0, h-1 do
			used[x+ix][y+iy] = true
		end
	end
end

local function row(image, used, x, w, y)
	for _x = x, x + w - 1 do
		if not gp(image, used, _x, y) then
			return false
		end
	end

	return true
end

local function eat(image, used, x, y)
	local w, h = image:getDimensions()
	
	local ix, iy = 0, 0
	while (x + ix) < w and gp(image, used, x+ix, y + iy) do
		ix = ix + 1
	end

	iy = 1

	while (y + iy) < h and row(image, used, x, ix, y + iy) do
		iy = iy + 1
	end

	use(used, x, ix, y, iy)

	return ix, iy
end

function patterns:spawn()
	local pat_type = math.random()

	local rare_chance = .01
	local hard_chance = .10 * S.difficulty

	if pat_type < rare_chance then
		pat_type = "rare"
	elseif S.score and S.score > 16 and pat_type < rare_chance + hard_chance then
		pat_type = "hard"
	else
		pat_type = "normal"
	end

	local pool = self[pat_type]
	local fname = pool[math.random(#pool)]
	if fname == self.last then
		fname = pool[math.random(#pool)] -- reroll once
	end
	self.last = fname
	--local fname = "random_3.png"
	--print("spawning " .. fname)

	local image = self.data[fname]

	local used = setmetatable({}, {__index = function(t, k) t[k] = {} return t[k] end})
	local w, h = image:getDimensions()
	local x, y = 0, 0

	-- fix bug where first block doesn't go as fast. iunno

	local offscreen = h * 36
	while y < h do
		while x < w do
			if not used[x][y] then
				assert(x >= 0, tostring(x))
				assert(x < w, tostring(x))
				assert(y >= 0, tostring(y))
				assert(y < h, tostring(y))
				if white(image:getPixel(x, y)) then
					local bw, bh = eat(image, used, x, y)

					table.insert(S.blocks, Block.new(x * 36, (y * 36) - offscreen, bw * 36, bh * 36))
				elseif black(image:getPixel(x, y)) then
					local bw, bh = 1, 1
					table.insert(S.blocks, Coin.new(x * 36, (y * 36) - offscreen, bw * 36, bh * 36))
				end
			end
			x = x + 1
		end
		x = 0
		y = y + 1
	end
end

return patterns
