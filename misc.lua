local class   = require 'gabe.class'

local x = {}

-- {{{ Higher order functions

function x.comp(...)
	local fns = {...}
	-- TODO: reuse results table
	return function(...)
		local results = {...}
		for i=#fns, 1, -1 do
			results = {fns[i](...)}
		end
		return unpack(results)
	end
end

function x.juxt(...)
	local fns = {...}
	return function(...)
		for _, f in ipairs(fns) do
			f(...)
		end
	end
end

function x.bind1(f,v)
    f = function_arg(f)
    return function(...)
        return f(v,...)
    end
end

function x.method(t, n)
	return function(...) return t[n](t, ...) end
end

--  }}}

-- {{{ table-related

--- Creates a table with a "default" value. if a key does not exist, `def` is
-- returned instead of `nil`.
function x.default(t)
	return function(def)
		return setmetatable(t, {__index = function() return def end})
	end
end

--- Set constructor. Builds a table `set` such that for each value `v` of the
-- input, set[v] == true
function x.set(...)
	local t = {}
	for i=1, select('#', ...) do
		t[select(i, ...)] = true
	end
	return t
end

function x.keys(t)
	local ks = {}
	for k, _ in pairs(t) do
		table.insert(ks, k)
	end
	return ks
end

function x.is_empty(t)
	for _, _ in pairs(t) do
		return false
	end
	return true
end

function x.has_val(t, v)
	for i, iv in ipairs(t) do
		if v == iv then
			return i
		end
	end
	return nil
end

function x.map(f, ...)
	local t = {}
	for k, v in ... do
		t[k] = f(v)
	end
	return t
end

-- }}}

-- {{{ object-related
local inspect_t = {
	process = function(item, path)
		if path[#path] == inspect.METATABLE and item.name then
			return {item.name}
		end
		return item
	end
}

-- class oriented variant
function x.inspect(o)
	local inspect = require 'inspect'
	return inspect(o, inspect_t)
end

function x.round(o)
	return math.floor(o + 0.5)
end

function x.lerp(a, b, t)
	return ((1-t) * a) + (t * b)
end

function x.coords(o)
	return o.x, o.y
end

function x.size(o)
	return o.w, o.h
end

function x.bbox(o)
	return o.x, o.y, o.w, o.h
end

function x.center(o)
	return o.x + o.w*.5, o.y + o.h*.5
end

function x.in_box(mx, my, x, y, w, h)
	return (mx >= x and mx <= x + w) and
	       (my >= y and my <= y + h)
end
-- }}}

-- {{{ geometry
function x.dist(x1, y1, x2, y2)
	local x, y = x2 - x1, y2 - y1
	return math.sqrt(x*x + y*y)
end

function x.dist2(x1, y1, x2, y2)
	local x, y = x2 - x1, y2 - y1
	return x*x + y*y
end

function x.len(x, y)
    return math.sqrt(x*x + y*y)
end

function x.len2(x, y)
    return x*x + y*y
end
-- }}}

function x.split(str, sep)
	local t = {}  -- NOTE: use {n = 0} in Lua-5.0
	local fpat = "(.-)" .. pat
	local last_end = 1
	local s, e, cap = str:find(fpat, 1)
	while s do
		if s ~= 1 or cap ~= "" then
			table.insert(t,cap)
		end
		last_end = e+1
		s, e, cap = str:find(fpat, last_end)
	end
	if last_end <= #str then
		cap = str:sub(last_end)
		table.insert(t, cap)
	end
	return t
end

function x.cmatch(msg, n)
	local i = 0
	for _ in string.gmatch(msg, n) do
		i = i + 1
	end
	return i
end

-- "round" increment-decrement
function x.rinc(i, len)
	return i % len + 1
end

function x.rdec(i, len)
	return (i - 2) % len + 1
end

x.Counter = class 'misc.counter'

function x.Counter:init(prefix)
	self.prefix = prefix or ""
	self.next_i = 1
end

function x.Counter:next()
	local s = self.prefix .. "." .. self.next_i
	self.next_i = self.next_i + 1
	return s
end

return x
