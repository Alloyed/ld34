Ludum Dare 34
=============

This is the game source for my LD34 entry, Tiny Chase DJ.
Since my build setup is a bit overcomplicated if you just want to check
out my source I'd suggest downloading the "Working Copy Zip" below:

http://alloyed.me/shared/games/ld34-source.zip

It's the same thing as this repo, just with all the dependencies and
stuff already checked out. For my own notes, (and yours if you're
masochistic enough):
```
$ git clone <url>
$ git annex init "my repo"
$ git annex get .
$ loverocks deps
$ love .

```

tools needed: git, git-annex, loverocks, love 0.9.2, oggenc

Itch page: http://alloyed.itch.io/ld34

LD Entry: http://ludumdare.com/compo/ludum-dare-34/?uid=26490


During-LD Notes
========

THEME: Two Button Controls and Growing, Pick both or either

Idea: rhythm game, similar to super-hexagon. you grow in size the
further you get.

Idea: avoidance game like undertale, stuck to an axis. no grow theme.

first two are the same idea :p

Idea: two buttons control all enemies. pixel platformer

Idea: grow a plant, two buttons influence the direction it grows.
```
w4ffles | I know something that grows but it's NSFW
w4ffles | ...
```

NOTES

More juice, bigger juice

More music, louder music


NAMES

Double Funk of Might and Magic

Irish Magic Colosseum

Tiny Chase DJ
Close enough and I'm too lazy to make a better one