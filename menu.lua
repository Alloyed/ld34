local class     = require 'gabe.class'
local Button    = require 'ui.button'
local Group     = require 'ui.group'
local Screen    = require 'letterbox'
local palette   = require 'palette'
local lg        = require 'love.graphics'

local menu = class 'menu'

local Buttons = {
	Button.extend("Play", function() Reset() swap(S.game) end),
	Button.extend("Play Hard Mode", function() Config.hardmode = true Reset() swap(S.game) end),
	Button.extend("High scores", function() end),
	Button.extend("Options", function() end),
	Button.extend("Quit", love.event.quit),
}

function menu:start()
	local w, h = Screen.dimensions()
	local bs, x, y = {}, w * .5, 100
	local h = A.menu_font:getHeight()
	for _, Btn in ipairs(Buttons) do
		table.insert(bs, Btn.new(x, 400))
	end
	self.buttons = Group.new(bs)
	A.screen.bgcolor = palette.background_1
end

function menu:update(dt)
	local w, h = Screen.dimensions()
	local id = love.keyboard.isDown
	local left = id'left' or id'a'
	local right = id'right' or id'd'

	if love.touch then
		for _, touch in ipairs(love.touch.getTouches()) do
			local x, y = A.screen:mouse(love.touch.getPosition(touch))
			if x < w * .4 then
				left = true
			elseif x > w * .6 then
				right = true
			end
		end
	end

	for _, joy in ipairs(love.joystick.getJoysticks()) do
		left  = left or joy:isGamepadDown('leftshoulder')
		right = right or joy:isGamepadDown('rightshoulder')
	end

	self.buttons:update(dt, left, right)
end

local function lines(c)
	local w, h = Screen.dimensions()
	lg.push()
		lg.setColor(palette.background_2)
		for i=1, 5 do
			lg.rectangle('fill', 0, 0, 36, h)
			lg.translate(72, 0)
		end
	lg.pop()
end

function menu:draw()
	lines()
	love.graphics.setFont(A.dbg_font)
	if Config.hardmode then
		love.graphics.print("* Hard mode enabled! *", 30, 300)
	end

	if Config.bloom then
		love.graphics.print("* Bloom enabled! *", 30, 320)
	end

	self.buttons:draw()
end

function menu:keyreleased(k)
	if k == 'return' or k == 'space' then
		print("yee")
		self.buttons.buttons[self.buttons.i].event()
	end
end

function menu:textinput(k)
	if k == 'h' or k == 'H' then
		Config.hardmode = not Config.hardmode
	elseif k == 'b' or k == 'B' then
		Config.bloom = not Config.bloom
	end
end

return menu
