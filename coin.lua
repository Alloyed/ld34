local class = require 'gabe.class'
local Screen = require 'letterbox'
local lg = require 'love.graphics'

local Coin = class 'coin'

Coin.size = 36
Coin.isCoin = true
--Coin.isBlock = true

local palette = require 'palette'
Coin.colors = palette.coin

function Coin:init(x, y, w, h, c)
	self.x, self.y = x, y
	self.w, self.h = w, h
	self.color = c or Coin.colors[math.random(#Coin.colors)]

	S.world:add(self, self.x, self.y, self.w or Coin.size, self.h or Coin.size)
end

function Coin.filter(item, other)
	if other.isPlayer then
		return 'cross'
	end
end

function Coin:update(dt,c)
	local _, h = Screen.dimensions()
	local v = 150
	self.y = self.y + v * dt
	if self.y > h + 500 then
		self:remove()
	end

	local ax, ay, cols, len = S.world:move(self, self.x, self.y, Coin.filter)
	if len ~= 0 and not S.player.dead then
		self:remove()
		S.score = S.score + 8
		local s = love.audio.newSource(A.pickup)
		s:setVolume(.75)
		s:play()
	end
	self.x, self.y = ax, ay
end

function Coin:remove()
	for i, b in ipairs(S.blocks) do
		if b == self then
			table.remove(S.blocks, i)
			return true
		end
	end
	return false
end

function Coin:draw_trail(c)
end

function Coin:draw(c)
	local w, h = self.w or Coin.size, self.h or Coin.size
	local r2 = math.sqrt(2)
	local dw, dh = w/r2, h/r2
	local cx, cy = self.x + w*.5, self.y + w*.5
	lg.setColor(unpack(self.color))
	lg.push()
	lg.translate(cx, cy)
	lg.rotate(c)
	lg.rectangle('fill', -dw*.5, -dh*.5, dw, dh)
	lg.pop()
end

return Coin
