package = "LD34"
version = "scm-1"
source = {
	url = "none"
}
description = {
	homepage = "*** please enter a project homepage ***",
	license = "*** please specify a license ***"
}
dependencies = {
	"lua ~> 5.1",
	"bump ~> 3",
	"dkjson ~> 2.5",
	"repler",
	"fun-alloyed",
	"gabe",
}
build = { type = "none" }
