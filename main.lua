require 'gabe'()
local class = require 'gabe.class'
local state = require 'gabe.state'
local reload = require 'gabe.reload'
local json = require 'dkjson'
local lg = require 'love.graphics'

_G.Reset = function()
	reload.reload_all()
	state.newA()
	return state.reset()
end

_G.Reload = function()
	reload.reload_all()
	state.newA()
end

Config = Config or {}
Config.player_w    = 20
Config.player_h    = 20
Config.player_y    = 500

Config.starting_difficulty = 1
Config.max_difficulty      = 3
Config.difficulty_step     = .05

Config.spawn_rate          = 4

Config.sample_rate = 48000
Config.bpm         = 130

Config.username    =  Config.username or"Gaston"

if Config.godmode == nil then
	Config.godmode = false
end

if Config.hardmode == nil then
	Config.hardmode = false
end

if Config.bloom == nil then
	Config.bloom = false
end

function love.load()
	state.init()
	state.newA()
	state.start()
	require 'repler'.load()
end

local Patterns = require 'patterns'
local Screen   = require 'letterbox'

function make_debug_font(name)
	local f = love.graphics.newImageFont(
		name,
		" abcdefghijklmnopqrstuvwxyz" ..
		"ABCDEFGHIJKLMNOPQRSTUVWXYZ0" ..
		"123456789.,!?-+/():;%&`'*#=[]\"_{}<>@←→↑↓")
	f:setFilter('nearest', 'nearest')

	return f
end

function state.newA()
	A.song      = A.song or love.audio.newSource("music/ld34.ogg", "stream")
	A.song_loop = A.song_loop or love.audio.newSource("music/ld34_loop.ogg", "stream")
	A.pickup    = A.pickup or love.sound.newSoundData("music/pickup.ogg")
	A.ded       = A.ded or love.sound.newSoundData("music/ded.ogg")
	A.hit_wall  = A.hit_wall or love.audio.newSource("music/hit_wall.ogg", "static")
	A.particle  = A.particle or love.graphics.newImage("art/pixel.png")
	A.patterns  = A.patterns or Patterns.new()
	A.menu_font = A.menu_font or love.graphics.newFont("art/upheavtt.ttf", 32)
	A.dbg_font  = A.dbg_font or make_debug_font("art/font.png")

	if Config.bloom then
		require 'bloom'
		A.bloom     = A.bloom or CreateBloomEffect(360, 640)
	end

	if love.system.getOS() == "Android" then
		A.screen    = A.screen or Screen.new {
			w = 360,
			h = 640,
			perfect = false,
			filter_min = 'linear',
			filter_max = 'linear'
		}
	else
		A.screen    = A.screen or Screen.new {
			w = 360,
			h = 640,
		}
	end
end

function state.stop()
	love.audio.stop()
end

local Game = require 'game'
local Menu = require 'menu'
function state.start()
	S.high_score = love.filesystem.load("high-score.lua")()
	S.game = Game.new()
	S.menu = Menu.new()
	swap(S.menu)
	S.muted = false
	S.msg = ""
end

function swap(o)
	if S.mode and S.mode.stop then
		S.mode:stop()
	end
	S.mode = o
	if S.mode.start then
		S.mode:start()
	end
end

-- this is off. I dunno why.
local coef = Config.bpm / 60 -- beats per second
coef = coef * Config.sample_rate -- beats per sample
coef = coef / 4

local old = 5544 * 4

function clock()
	if A.song:isPlaying() then
		return A.song:tell('samples') / 22000
	else
		return A.song_loop:tell('samples') / 22000
	end
end

function translate_dt(dt)
	local samples = dt * Config.sample_rate * A.song:getPitch()
	return samples / 22000
end

function love.update(dt)
	if S.channel then
		local t = S.channel:pop()
		if t and t.json then
			S.channel = nil
			S.leaderboard = json.decode(t.json)
		elseif t and t.err then
			S.channel = nil
			if t.err == 'connection refused' then
				error("Connection refused. The server is probably offline.")
			end
			error(t.err)
		end
	end
	S.mode:update(dt)
end

function love.draw()
	A.screen:start()
	S.mode:draw()
	A.screen:stop()
end

local function send_score()
	local th = love.thread.newThread("request.lua")
	S.channel = love.thread.newChannel()
	th:start(S.channel, Config.username, S.score)
end

function love.keypressed(k)
	if S.mode and S.mode.keypressed and S.mode:keypressed(k) then
		return
	end
end

function love.keyreleased(k)
	if S.mode and S.mode.keyreleased and S.mode:keyreleased(k) then
		return
	end
end

local function space()
	if S.player and S.player.dead then
		Reset()
		swap(S.game)
	elseif S.player then
		S.paused = not S.paused
		if S.paused then
			love.audio.pause()
		else
			love.audio.resume()
		end
	end
end

function love.textinput(k, ...)
	S.msg = "keypress ".. k .. "\n" .. S.msg
	if S.mode and S.mode.textinput and S.mode:textinput(k) then
		return
	elseif k == 'R' then
		Reset()
	elseif k == 'r' then
		Reload()
	elseif k == 'm' then
		S.muted = not S.muted
		love.audio.setVolume(S.muted and 0.0 or 1.0)
	elseif k == ' ' then
		space()
	elseif k == 'u' and S.player and S.player.dead and not S.channel then
		S.name = Config.username
		love.keyboard.setKeyRepeat(true)
	end
end

function love.touchpressed(id, x, y, pressure)
	if S.player and S.player.dead then
		Reset()
		swap(S.game)
	end
end

function love.gamepadreleased(joy, btn)
	if btn == 'a' then
		space()
	elseif btn == 'back' and S.player and S.player.dead and not S.channel then
		S.name = Config.username
		love.keyboard.setKeyRepeat(true)
	end
end

function love.focus(f)
	if not f and S.player and not S.player.dead then
		S.paused = true
	end
end

function love.threaderror(thread, errorstr)
  print("Thread error!\n"..errorstr)
  S.channel = nil
  -- thread:getError() will return the same error string now.
end
