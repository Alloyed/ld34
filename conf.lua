if love.filesystem then
	require 'rocks' ()
	love.filesystem.setSymlinksEnabled(true)
end

function love.conf(t)
	t.identity = "alloyed_LD34" -- the chance of someone else doing LD34 is pretty high tbh
	t.window.title = "Tiny Chase DJ"
	if love._version_minor == 10 then
		t.version = "0.10.0"
	else
		t.version = "0.9.2"
	end

	t.window.width = 360
	t.window.height = 640
	t.window.vsync = false
end
