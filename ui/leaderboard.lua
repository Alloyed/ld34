
local function printc(s, x, y, f)
	if not f then
		f = love.graphics.getFont()
	end
	local fw = f:getWidth(s)
	lg.print(s, x - fw*.5, y)
end

local function draw_leaderboard(controls, w)
	if not S.leaderboard then
		printc("Retrieving\nleaderboad...", 180, 150)
		if not S.channel then
		end
	end
	lg.push()

	lg.setFont(A.dbg_font)
	lg.translate(0, 200)
	local board  = S.leaderboard.scores
	local our_id = S.leaderboard["your-id"]
	local max = math.min(#board, 28)
	for i=1, max do
		local rank = board[i]
		if our_id == rank.id then
			lg.setColor(palette.selected)
			lg.rectangle('fill', 5, 0, w - 10, 12)
		elseif i % 2 == 1 then
			lg.setColor(100, 100, 100, 100)
			lg.rectangle('fill', 5, 0, w - 10, 12)
		end

		lg.setColor(255, 255, 255)
		local name = string.format("%02d. %s", i, rank.username)
		lg.print(name, 10, 0)
		local score = string.format("%.02f", rank.score)
		local fw = A.dbg_font:getWidth(score)
		lg.print(score, w - fw- 10, 0)
		lg.translate(0, 12)
	end
	lg.setFont(A.menu_font)
	printc(controls, 180, 10)
	lg.pop()
end

return draw_leaderboard
