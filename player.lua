local class = require 'gabe.class'
local lg = require 'love.graphics'
local Screen = require 'letterbox'
local flux = require 'flux'

local Player = class 'player'

Player.isPlayer = true

function Player:init(x, y, w, h)
	self.cx = x
	self.cy = y
	self.w = w
	self.h = h
	self.draw_w = w
	self.draw_h = h
	self.dead = false
	self.last_crouched = false
	self.moving = 0

	if not Config.godmode then
		S.world:add(self, self:aabb())
	end
end

function Player.filter(item, other)
	if type(other) == 'string' or other.isBlock then
		return "slide"
	end
end

local function round(x, increment)
	if increment then return round(x / increment) * increment end
	return x >= 0 and math.floor(x + .5) or math.ceil(x - .5)
end


function Player:update(dt, c)
	if self.shake then
		self.shake = self.shake - dt
		if self.shake < 0 then
			self.shake = nil
		end
	end
	local id = love.keyboard.isDown
	local w, _ = Screen.dimensions()
	local v = w * dt * .5

	local left = id'left' or id'a'
	local right = id'right' or id'd'

	if love.touch then
		for _, touch in ipairs(love.touch.getTouches()) do
			local x, y = A.screen:mouse(love.touch.getPosition(touch))
			if x < w * .4 then
				left = true
			elseif x > w * .6 then
				right = true
			end
		end
	end

	for _, joy in ipairs(love.joystick.getJoysticks()) do
		left  = left or joy:isGamepadDown('leftshoulder')
		right = right or joy:isGamepadDown('rightshoulder')
	end

	local crouched = false

	local old_cx = self.cx
	if left and right then
		crouched = true
		self.cx = round(self.cx - 18, 36) + 18
	elseif left then
		self.cx = self.cx - v
	elseif right then
		self.cx = self.cx + v
	end
	--self.cx = math.min(math.max(self.w*.5, self.cx), 360-self.w*.5)
	self.left  = left
	self.right = right

	if crouched ~= self.last_crouched then
		-- resize
		self.w = Config.player_w * (crouched and 1 or S.difficulty)
		self.h = Config.player_h * (crouched and 1 or S.difficulty)
		self:resize()
	end

	self.last_crouched = crouched

	if not Config.godmode then
		local gx, gy = self:aabb()
		local ax, ay, cols, len = S.world:move(self, gx, gy, Player.filter)

		if len ~= 0 and self.moving > .5 then
			S.hshake = .25
			A.hit_wall:stop()
			A.hit_wall:rewind()
			A.hit_wall:play()
			self.moving = 0
		end

		self.cx = ax + self.w * .5
		self.cy = ay + self.w * .5
	end

	if math.abs(self.cx - old_cx) > .001 then
		self.moving = self.moving + dt
	else
		self.moving = 0
	end
end

function Player:draw(c)
	local palette = require 'palette'
	local color1 = palette.player
	local color2 = palette.player_glow
	local color3 = palette.player_crouched
	local pw, ph = self.draw_w, self.draw_h
	lg.push()
		lg.translate(self.cx, self.cy)
		lg.push()
			lg.setColor(unpack(color2))
			lg.scale(2 - (c - math.floor(c)), 2 - (c - math.floor(c)))
			lg.rectangle('fill', pw * -.5, ph * -.5, pw, ph)
		lg.pop()

		if self.dead then
			lg.pop() return 
		elseif self.last_crouched then
			lg.setColor(unpack(color3))
		else
			lg.setColor(unpack(color1))
		end
		lg.rectangle('fill', pw * -.5, ph * -.5, pw, ph)
	lg.pop()
end

function Player:aabb()
	return self.cx - self.w * .5, self.cy - self.h * .5, self.w, self.h 
end

function Player:resize()
	if not Config.godmode then
		S.world:update(self, self:aabb())
	end
	flux.to(self, .125, {draw_w = self.w, draw_h = self.h})
end

return Player
