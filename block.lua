local class = require 'gabe.class'
local Screen = require 'letterbox'
local lg = require 'love.graphics'

local Block = class 'block'

Block.size = 36
Block.isBlock = true

local palette = require 'palette'
Block.colors = palette.block_colors

function Block:init(x, y, w, h, c)
	self.x, self.y = x, y
	self.w, self.h = w, h
	self.color = c or Block.colors[math.random(#Block.colors)]

	self.trail = love.graphics.newParticleSystem(A.particle, 100)
	self.trail:setPosition(self.x + self.w * .5, self.y)
	self.trail:setEmissionRate(5)
	self.trail:setParticleLifetime(0, 5)
	--local s = 100
	self.trail:setAreaSpread("uniform", self.w * .5, 10)
	-- Fade to transparency.
	self.trail:setColors(
		self.color[1], self.color[2], self.color[3], 0x66,
		self.color[1], self.color[2], self.color[3], 0)

	self.trail:setSpeed(10, 20)
	self.trail:setDirection(math.pi/2)

	S.world:add(self, self.x, self.y, self.w or Block.size, self.h or Block.size)
end

function Block.filter(item, other)
	if other.isPlayer then
		return 'cross'
	end
end

function Block:update(dt,c)
	local _, h = Screen.dimensions()
	local v = 150
	self.y = self.y + v * dt
	if self.y > h + 500 then
		self:remove()
	end

	local ax, ay, cols, len = S.world:move(self, self.x, self.y, Block.filter)
	if len ~= 0 then
		for _, col in ipairs(cols) do
			if not Config.godmode and not S.player.dead then
				local s = love.audio.newSource(A.ded)
				s:setVolume(.75)
				s:play()
				S.vshake = .25
				S.player.dead = c
				A.song:setPitch(.5)
				A.song_loop:setPitch(.5)
				A.song:stop()
				A.song_loop:stop()
				if S.score > S.high_score then
					love.filesystem.write("high-score.lua", string.format("return %f", S.score))
				end
			end
		end
	end
	self.x, self.y = ax, ay
	self.trail:moveTo(self.x + self.w * .5, self.y)
	self.trail:update(dt)
end

function Block:remove()
	for i, b in ipairs(S.blocks) do
		if b == self then
			table.remove(S.blocks, i)
			return true
		end
	end
	return false
end

function Block:draw_trail(c)
	lg.setColor(255, 255, 255)
	lg.draw(self.trail)
end

local function round(x, increment)
	if increment then return round(x / increment) * increment end
	return x >= 0 and math.floor(x + .5) or math.ceil(x - .5)
end


function Block.stencil()
	local _, h = Screen.dimensions()
	local cx, w, cy = S.player.cx, S.player.w, S.player.cy
	local x0, x1 = cx - w*.5, cx + w*.5
	local _x0 = round(x0 - 18, 36)
	local _x1 = round(x1 - 18, 36) + 36
	lg.rectangle('fill', x0, 0, x1 - x0, cy)
end

function Block:draw(c)
	local w, h = self.w or Block.size, self.h or Block.size
	lg.setColor(self.color)
	lg.rectangle('fill', self.x, self.y, w, h)
	lg.setStencil(Block.stencil)
	lg.setColor(palette.block_highlight)
	lg.rectangle('fill', self.x, self.y, w, h)
	lg.setStencil()
end

return Block
