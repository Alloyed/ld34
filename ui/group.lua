local class = require 'gabe.class'
local misc  = require 'misc'

local flux = require 'flux'

local Group = class 'ui.group'

function Group:init(buttons)
	self.buttons  = buttons
	self.i        = 1
	self.axisWait = 0

	for i, btn in ipairs(self.buttons) do
		if i == 1 then
			btn.x = 180 - (btn.w * .5)
		elseif i == 2 then
			btn.x = 330
		elseif i == #self.buttons then
			btn.x = -btn.w + 30
		else
			btn.x = -300
		end
	end
end

function Group:update(dt, left, right)
	flux.update(dt)
	if not left and not right then
		self.axisWait = 0
	end

	if self.axisWait < 0 then
		if left then
			self.axisWait = .48
			self:prev()
		elseif right then
			self.axisWait = .48
			self:next()
		end
	else
		self.axisWait = self.axisWait - dt
	end

	local selected
	for i, btn in ipairs(self.buttons) do
		if btn:update(nil, dt, x, y) then
			self.i = i
			selected = true
		end
	end
	if selected then return true end

	self.buttons[self.i].on = true
end

function Group:draw()
	local len = #self.buttons

	love.graphics.setColor(255, 255, 255)
	local btn = self.buttons[self.i]
	love.graphics.rectangle('fill', 0, btn.y, 360, btn.h)

	love.graphics.setColor(0, 0, 0)

	local btn = self.buttons[self.i]
	btn:draw()

	btn = self.buttons[misc.rinc(self.i, len)]
	btn:draw()

	btn = self.buttons[misc.rdec(self.i, len)]
	btn:draw()
end

function Group:next()
	local d = .48
	local len = #self.buttons

	local btn = self.buttons[self.i]
	if not btn then
		self.i = misc.rinc(self.i, #self.buttons)
		return
	end

	btn.x = 180 - btn.w * .5
	flux.to(btn, d, { x = -btn.w + 30 }):ease("sineinout")

	btn = self.buttons[misc.rinc(self.i, len)] -- +1
	btn.x = 330
	flux.to(btn, d, { x = 180 - (btn.w * .5) }):ease("sineinout")

	self.i = misc.rinc(self.i, #self.buttons)
	btn = self.buttons[misc.rinc(self.i, len)] -- +1
	btn.x = 400
	flux.to(btn, d, { x = 330 }):ease("sineinout")
end

function Group:prev()
	local d = .48
	local len = #self.buttons

	local btn = self.buttons[self.i]
	if not btn then
		self.i = misc.rinc(self.i, #self.buttons)
		return
	end

	flux.to(btn, d, { x = 330 })

	btn = self.buttons[misc.rdec(self.i, len)] -- +1
	flux.to(btn, d, { x = 180 - (btn.w * .5) })

	self.i = misc.rdec(self.i, #self.buttons)
	btn = self.buttons[misc.rdec(self.i, len)] -- +1
	btn.x = -400
	flux.to(btn, d, { x = -btn.w + 30 })
end

function Group:mousereleased(x, y, b)
	for i, btn in ipairs(self.buttons) do
		if btn:click(x, y, b) then
			btn.event()
			return true
		end
	end
	return false
end

function Group:keyreleased(k)
	if k == 'up' then
		self:prev()
	elseif k == 'down' then
		self:next()
	elseif k == 'x' or k == 'return' then
		self.buttons[self.i].event()
	else
		return false
	end
	return true
end

function Group:gamepadreleased(pad, k)
	if k == 'dpup' then
		self:prev()
	elseif k == 'dpdown' then
		self:next()
	elseif k == 'a' or k == 'start' then
		self.buttons[self.i].event()
	else
		return false
	end
	return true
end

return Group
