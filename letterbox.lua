local class = require 'gabe.class'

local Letterbox = class 'gfx.letterbox'

function Letterbox:init(config)
	local cv = love.graphics.newCanvas(config.w, config.h, config.format, config.msaa)
	cv:setFilter(config.filter_min or 'nearest', config.filter_max or 'nearest')
	self.canvas  = cv
	self.scale   = config.scale
	self.perfect = config.perfect
	if self.perfect == nil then
		self.perfect = true
	end
	self.bgcolor = config.bgcolor or {255, 255, 255}
	self.shader  = config.shader
end

function Letterbox:resize(w, h)
	local min, mag = self.canvas:getFilter()
	local fmt = self.canvas:getFormat()
	local msaa = self.canvas:getMSAA()

	local cv = love.graphics.newCanvas(w, h, fmt, msaa)
	cv:setFilter(min, mag)
	self.canvas = cv
end

function Letterbox:start()
	love.graphics.setCanvas(self.canvas)
	if self.canvas.clear then -- 0.9
		love.graphics.setBlendMode('alpha')
		self.canvas:clear(self.bgcolor) -- 0.9
	else -- 0.10
		love.graphics.setBlendMode('alpha', true)
		love.graphics.clear(unpack(self.bgcolor))
	end
end

local function canvas_scale(perfect, bigw, bigh, smallw, smallh)
	local w, h
	if perfect then
		w = math.floor(bigw / smallw)
		h = math.floor(bigh / smallh)
	else
		w = bigw / smallw
		h = bigh / smallh
	end
	return (w < h) and w or h
end

function Letterbox:stop()
	local winw, winh = love.graphics.getDimensions()
	local canvw, canvh = self.canvas:getDimensions()
	local scale = self.scale or canvas_scale(self.perfect, winw, winh, canvw, canvh)
	local cx, cy = math.floor(winw/2), math.floor(winh/2)
	local hw, hh = canvw/2, canvh/2

	love.graphics.setCanvas()
	if self.canvas.clear then
		love.graphics.setBlendMode('premultiplied')
	else
		love.graphics.setBlendMode('alpha', false)
	end
	love.graphics.setColor(255, 255, 255)
	love.graphics.setShader(self.shader)
	love.graphics.draw(self.canvas, cx, cy, nil, scale, nil, hw, hh)
	love.graphics.setShader()
end

function Letterbox:mouse(mx, my)
	if not mx then
		mx, my = love.mouse.getPosition()
	end

	local winw, winh = love.graphics.getDimensions()
	local canvw, canvh = self.canvas:getDimensions()
	local scale = self.scale or canvas_scale(self.perfect, winw, winh, canvw, canvh)

	-- total margin
	local marginx, marginy = winw - (canvw * scale), winh - (canvh * scale)

	mx = (mx / scale) - (marginx / (2 * scale))
	my = (my / scale) - (marginy / (2 * scale))
	return mx, my
end

function Letterbox.dimensions()
	return 360, 640 -- WOO
	--local cv = love.graphics.getCanvas()
	--if cv then
	--	return cv:getDimensions()
	--else
	--	return love.window.getDimensions()
	--end
end

return Letterbox
